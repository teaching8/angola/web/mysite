from django.contrib import admin

from .models import Choice, Question


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 1


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('title', 'text', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['text']
    fieldsets = [
        (None, {'fields': ['title', 'text', 'user']}),
        ('Date information', {'fields': ['pub_date']}),
    ]
    inlines = [ChoiceInline]

admin.site.register(Question, QuestionAdmin)
